/*
Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Package quantumLeap provides a client for interacting with the FIWARE QuantumLeap

package quantumLeap

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

// Client implements a client for the QuantumLeap client
type Client struct {
	url     string       // url of QuantumLeap
	httpCli *http.Client // http client
}

// NewClient creates a new QuantumLeap client
func NewClient(url string) (cli *Client, err error) {
	cli = &Client{
		url: url + "/v2",
		httpCli: &http.Client{
			Timeout: time.Second * 10,
		},
	}
	return
}

// GetEntityID requests the list of all the entityID
func (cli *Client) GetEntityID(paras map[string]string, tenant string) (ent EntityID, err error) {
	var body []byte
	body, err = cli.get("/entities", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetAttribute requests history of an attribute of a given entity instance
func (cli *Client) GetAttribute(entID string, attrName string, paras map[string]string, tenant string) (res AttributeIndexedValues, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entID+"/attrs/"+attrName, paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAttributeVal requests History of an attribute (values only) of a given entity instance
func (cli *Client) GetAttributeVal(entID string, attrName string, paras map[string]string, tenant string) (res IndexedValues, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entID+"/attrs/"+attrName+"/value", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAttributes requests history of N attributes of a given entity instance
func (cli *Client) GetAttributes(entID string, paras map[string]string, tenant string) (res AttributesIndexedValue, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entID, paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAttributesVal requests history of N attributes (values only) of a given entity instance
func (cli *Client) GetAttributesVal(entID string, paras map[string]string, tenant string) (res AttributesIndexedValueShort, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entID+"/value", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetTypeAttribute requests history of an attribute of N entities of the same type
func (cli *Client) GetTypeAttribute(entType string, attrName string, paras map[string]string, tenant string) (res TypeAttribute, err error) {
	var body []byte
	body, err = cli.get("/types/"+entType+"/attrs/"+attrName, paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetTypeAttributeVal requests history of an attribute(values only) of N entities of the same type
func (cli *Client) GetTypeAttributeVal(entType string, attrName string, paras map[string]string, tenant string) (res []EntityIndexedValues, err error) {
	var body []byte
	body, err = cli.get("/types/"+entType+"/attrs/"+attrName+"/value", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetTypeAttributes requests history of N attributes of N entities of the same type
func (cli *Client) GetTypeAttributes(entType string, paras map[string]string, tenant string) (res TypeAttributes, err error) {
	var body []byte
	body, err = cli.get("/types/"+entType, paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetTypeAttributesVal requests history of N attributes(values only) of N entities of the same type
func (cli *Client) GetTypeAttributesVal(entType string, paras map[string]string, tenant string) (res TypeAttributes, err error) {
	var body []byte
	body, err = cli.get("/types/"+entType+"/value", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAllAttribute requests history of an attribute of N entities of N types
func (cli *Client) GetAllAttribute(attrName string, paras map[string]string, tenant string) (res AllAttribute, err error) {
	var body []byte
	body, err = cli.get("/attrs/"+attrName, paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAllAttributeVal requests history of an attribute(values only) of N entities of N types
func (cli *Client) GetAllAttributeVal(attrName string, paras map[string]string, tenant string) (res AllAttribute, err error) {
	var body []byte
	body, err = cli.get("/attrs/"+attrName+"/value", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAllAttributes srequests history of N attributes of N entities of N types
func (cli *Client) GetAllAttributes(paras map[string]string, tenant string) (res []AllAttribute, err error) {
	var body []byte
	body, err = cli.get("/attrs", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// GetAllAttributesVal srequests history of N attributes of N entities of N types
func (cli *Client) GetAllAttributesVal(paras map[string]string, tenant string) (res []AllAttribute, err error) {
	var body []byte
	body, err = cli.get("/attrs/value", paras, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &res)
	return
}

// get sends one get request to the QuantumLeap client and returns the answer
func (cli *Client) get(path string, paras map[string]string, tenant string) (body []byte, err error) {
	var request *http.Request
	request, err = http.NewRequest("GET", cli.url+path, nil)
	q := request.URL.Query()
	for key, val := range paras {
		q.Add(key, val)
	}
	request.URL.RawQuery = q.Encode()
	if tenant != "" {
		request.Header.Set("Fiware-Service", tenant)
	}
	var resp *http.Response
	resp, err = cli.httpCli.Do(request)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = errors.New("http error: " + strconv.Itoa(resp.StatusCode))
		ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		return
	}
	body, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	return
}
