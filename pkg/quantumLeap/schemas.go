/*
Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package quantumLeap

// EntityID describes the List of all the entityId
type EntityID struct {
	ID    string   `json:"entityID"` // ID of entity
	Type  string   `json:"type"`     // Type of entity
	Index []string `json:"index"`    // Array of index
}

// AttributeIndexedValue describes the indexed values for an attribute of an entity
type AttributeIndexedValues struct {
	ID       string   `json:"entityID"` // ID of entity
	AttrName string   `json:"attrName"` // Name of the attribute
	Index    []string `json:"index"`    // Array of index
	Values   []string `json:"values"`   // Array of values
}

// IndexedValues describes the indexed values
type IndexedValues struct {
	Index  []string `json:"index"`  // Array of index
	Values []string `json:"values"` // Array of values
}

// AttributeValues describes values of an attribute
type AttributeValues struct {
	AttrName string   `json:"attrName"` // Name of the attribute
	Values   []string `json:"values"`   // Array of the Values
}

// AttributesIndexedValue describes the indexed values of attributes with the entityID
type AttributesIndexedValue struct {
	ID         string            `json:"entityID"`   // ID of entity
	Index      []string          `json:"index"`      // Array of index
	Attributes []AttributeValues `json:"attributes"` // Array of values
}

// AttributesIndexedValueShort describes the indexed values of attributes
type AttributesIndexedValueShort struct {
	Index      []string          `json:"index"`      // Array of index
	Attributes []AttributeValues `json:"attributes"` // Array of values
}

// EntityIndexedValues describes the indexed Values of an entity ID
type EntityIndexedValues struct {
	ID     string   `json:"entityID"`
	Index  []string `json:"index"`  // Array of index
	Values []string `json:"values"` // Array of values
}

// TypeAttribute describes the indexed Values of an attribute of an type
type TypeAttribute struct {
	EntityType string                `json:"entityType"`
	AttrName   string                `json:"attrName"`
	Entities   []EntityIndexedValues `json:"entities"`
}

// TypeAttribute describes the indexed Values of all attributes of an type
type TypeAttributes struct {
	EntityType string                   `json:"entityType"`
	Entities   []AttributesIndexedValue `json:"entities"`
}

// Type describes the indexed Values of a type
type Type struct {
	EntityType string                `json:"entityType"`
	Entities   []EntityIndexedValues `json:"entities"`
}

// AllAttribute describes the indexed Values of an attribute of all types
type AllAttribute struct {
	AttrName string `json:"attrName"`
	Types    []Type `json:"types"`
}
