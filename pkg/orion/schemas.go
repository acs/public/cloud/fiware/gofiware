/*
Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package orion

import (
	"encoding/json"
	"errors"
	"time"
)

// Entity describes an antity stored in the context broker
type Entity struct {
	ID         string `json:"id"`   // ID of entity
	Type       string `json:"type"` // Type of entity
	Attributes map[string]Attribute
}

// MarshalJSON is the custom json marshal for Entity
func (ent Entity) MarshalJSON() (ret []byte, err error) {
	js := "{\"id\":\"" + ent.ID + "\",\"type\":\"" + ent.Type + "\""
	for i := range ent.Attributes {
		js += ",\"" + i + "\":"
		var temp []byte
		temp, err = json.Marshal(ent.Attributes[i])
		if err != nil {
			return
		}
		js += string(temp)
	}
	js += "}"
	ret = []byte(js)
	return
}

// UnmarshalJSON is the custom json unmarshal for Entity
func (ent *Entity) UnmarshalJSON(b []byte) (err error) {
	ent.Attributes = make(map[string]Attribute)
	rawMap := make(map[string]json.RawMessage)
	err = json.Unmarshal(b, &rawMap)
	if err != nil {
		return
	}
	for i := range rawMap {
		if i == "id" {
			err = json.Unmarshal(rawMap[i], &ent.ID)
			if err != nil {
				return
			}
		} else if i == "type" {
			err = json.Unmarshal(rawMap[i], &ent.Type)
			if err != nil {
				return
			}
		} else {
			var attr Attribute
			err = json.Unmarshal(rawMap[i], &attr)
			if err != nil {
				return
			}
			ent.Attributes[i] = attr
		}
	}
	return
}

// EntityKV describes an entity with attributes as key values
type EntityKV struct {
	ID         string // ID of entity
	Type       string // Type of entity
	Attributes map[string]interface{}
}

// MarshalJSON is the custom json marshal for EntityKV
func (ent EntityKV) MarshalJSON() (ret []byte, err error) {
	js := "{\"id\":\"" + ent.ID + "\",\"type\":\"" + ent.Type + "\""
	for i := range ent.Attributes {
		js += ",\"" + i + "\":"
		var temp []byte
		temp, err = json.Marshal(ent.Attributes[i])
		if err != nil {
			return
		}
		js += string(temp)
	}
	js += "}"
	ret = []byte(js)
	return
}

// UnmarshalJSON is the custom json unmarshal for EntityKV
func (ent *EntityKV) UnmarshalJSON(b []byte) (err error) {
	ent.Attributes = make(map[string]interface{})
	rawMap := make(map[string]json.RawMessage)
	err = json.Unmarshal(b, &rawMap)
	if err != nil {
		return
	}
	for i := range rawMap {
		if i == "id" {
			err = json.Unmarshal(rawMap[i], &ent.ID)
			if err != nil {
				return
			}
			continue
		}
		if i == "type" {
			err = json.Unmarshal(rawMap[i], &ent.Type)
			if err != nil {
				return
			}
			continue
		}
		var val interface{}
		err = json.Unmarshal(rawMap[i], &val)
		if err != nil {
			return
		}
		switch v := val.(type) {
		case map[string]interface{}:
			ent.Attributes[i] = rawMap[i]
		case int, string, bool, float32, float64:
			ent.Attributes[i] = v
		default:
			ent.Attributes[i] = v
		}
	}
	return
}

// EntityShort describes an entity stored in the context broker without attributes
type EntityShort struct {
	ID   string `json:"id"`   // ID of entity
	Type string `json:"type"` // Type of entity
}

// Attribute describes the attribute of an entity
type Attribute struct {
	Value interface{} `json:"value"` // value of the attribute
	Type  string      `json:"type"`  // type of the attribute
}

// UnmarshalJSON is the custom json unmarshal for Attribute
func (attr *Attribute) UnmarshalJSON(b []byte) (err error) {
	attrMap := make(map[string]json.RawMessage)
	err = json.Unmarshal(b, &attrMap)
	if err != nil {
		return
	}
	typ, ok := attrMap["type"]
	if !ok {
		err = errors.New("JSON error: No attribute type for attribute")
		return
	}
	err = json.Unmarshal(typ, &attr.Type)
	if err != nil {
		return
	}
	temp, ok := attrMap["value"]
	if !ok {
		err = errors.New("JSON error: No attribute value for attribute")
		return
	}
	var val interface{}
	err = json.Unmarshal(temp, &val)
	if err != nil {
		return
	}
	switch v := val.(type) {
	case map[string]interface{}:
		// if non-standard type use json.RawMessage
		attr.Value = temp
	case int:
		attr.Value = v
	case string:
		attr.Value = v
	case bool:
		attr.Value = v
	case float64:
		attr.Value = v
		var tempint int
		err = json.Unmarshal(temp, &tempint)
		if err == nil {
			attr.Value = tempint
		}
		err = nil
	default:
		// else use value of standard type
		attr.Value = v
	}
	return
}

// AttributeList is a list of Attributes
type AttributeList struct {
	Attributes map[string]Attribute
}

// MarshalJSON is the custom json marshal for EntityKV
func (atr AttributeList) MarshalJSON() (ret []byte, err error) {
	js := "{"
	c := 0
	for i := range atr.Attributes {
		if c > 0 {
			js += ","
		}
		js += "\"" + i + "\":"
		var temp []byte
		temp, err = json.Marshal(atr.Attributes[i])
		if err != nil {
			return
		}
		js += string(temp)
		c++
	}
	js += "}"
	ret = []byte(js)
	return
}

// Subscription describes a subscription made to context broker
type Subscription struct {
	Description  string       `json:"description"`
	Subject      Subject      `json:"subject"`
	Notification Notification `json:"notification"`
	Expires      time.Time    `json:"expires"`
	Throttling   int          `json:"throttling"`
}

// Subject describes the subject of a subscription
type Subject struct {
	Entities  []EntityShort `json:"entities"`
	Condition Condition     `json:"condition"`
}

// Condition describes the attributes which trigger a notification
type Condition struct {
	Attributes []string `json:"attrs"`
}

// Notification describes the notification for a subscription
type Notification struct {
	HTTP       URL      `json:"http"`
	Attributes []string `json:"attrs"`
}

// URL describes the url used to send a notification
type URL struct {
	URL string `json:"url"`
}

// Type describes a type of entities registered with Context broker
type Type struct {
	Attributes map[string]AttrDescription `json:"attrs"`
	Count      int                        `json:"count"`
	Type       string                     `json:"type"`
}

// AttrDescription describes an attribute
type AttrDescription struct {
	Types []string `json:"types"`
}
