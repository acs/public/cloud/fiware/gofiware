/*
Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Package orion provides a client for interacting with the FIWARE Orion API
package orion

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

// Client implements a client for the orion context broker
type Client struct {
	url     string       // url of context broker
	httpCli *http.Client // http client
}

// NewClient creates a new orion client
func NewClient(url string) (cli *Client, err error) {
	cli = &Client{
		url: url + "/v2",
		httpCli: &http.Client{
			Timeout: time.Second * 10,
		},
	}
	return
}

// PostEntity posts a new entity to the context broker
func (cli *Client) PostEntity(ent Entity, tenant string) (err error) {
	_, err = cli.post("/entities", ent, tenant)
	return
}

// GetEntity requests an entity from the context broker
func (cli *Client) GetEntity(entName string, tenant string) (ent Entity, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entName, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetEntityKV requests an entity in short form
func (cli *Client) GetEntityKV(entName string, tenant string) (ent EntityKV, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entName+"/?options=keyValues", tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetEntityAttributes requests an entity's attributes
func (cli *Client) GetEntityAttributes(entName string, tenant string,
	attrs ...string) (values []interface{}, err error) {
	var body []byte
	path := "/entities/" + entName + "/?options=values&attrs="
	for i := range attrs {
		if i > 0 {
			path += ","
		}
		path += attrs[i]
	}
	body, err = cli.get(path, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &values)
	return
}

// GetAttribute requests a single attribute of an entity
func (cli *Client) GetAttribute(entName string, attrName string, tenant string) (attr Attribute, err error) {
	var body []byte
	body, err = cli.get("/entities/"+entName+"/attrs/"+attrName, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &attr)
	return
}

// GetAllEntities requests all entities from the context broker
func (cli *Client) GetAllEntities(tenant string) (ent []Entity, err error) {
	var body []byte
	body, err = cli.get("/entities", tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetAllEntitiesPage requests all entities in specified range
func (cli *Client) GetAllEntitiesPage(offset int, limit int, tenant string) (ent []Entity, err error) {
	var body []byte
	body, err = cli.get("/entities?offset="+strconv.Itoa(offset)+"&limit="+
		strconv.Itoa(limit), tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetAllEntitiesType requests all entities of a specific type
func (cli *Client) GetAllEntitiesType(typ string, tenant string) (ent []Entity, err error) {
	var body []byte
	body, err = cli.get("/entities?type="+typ, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetAllEntitiesTypePage requests all entities of a specific type
func (cli *Client) GetAllEntitiesTypePage(typ string, offset int, limit int,
	tenant string) (ent []Entity, err error) {
	var body []byte
	body, err = cli.get("/entities?type="+typ+"&offset="+strconv.Itoa(offset)+"&limit="+
		strconv.Itoa(limit), tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetAllEntitiesPattern requests all entities with matching id pattern
func (cli *Client) GetAllEntitiesPattern(pattern string, tenant string) (ent []Entity, err error) {
	var body []byte
	body, err = cli.get("/entities?idPattern="+pattern, tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// GetAllEntitiesPatternPage requests all entities with matching id pattern
func (cli *Client) GetAllEntitiesPatternPage(pattern string, offset int, limit int,
	tenant string) (ent []Entity, err error) {
	var body []byte
	body, err = cli.get("/entities?idPattern="+pattern+"&offset="+strconv.Itoa(offset)+
		"&limit="+strconv.Itoa(limit), tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &ent)
	return
}

// Todo get all entities with filtering

// UpdateAttributes updates attributes of an entity
func (cli *Client) UpdateAttributes(entName string, atr AttributeList, tenant string) (err error) {
	err = cli.patch("/entities/"+entName+"/attrs", atr, tenant)
	return
}

// UpdateEntity updates an entity already present in context broker
func (cli *Client) UpdateEntity(entName string, atr AttributeList, tenant string) (err error) {
	err = cli.put("/entities/"+entName+"/attrs", atr, tenant)
	return
}

// DeleteEntity deletes an entity
func (cli *Client) DeleteEntity(entName string, tenant string) (err error) {
	err = cli.delete("/entities/"+entName, tenant)
	return
}

// Subscribe subscribes to the context broker
func (cli *Client) Subscribe(sub Subscription, tenant string) (id string, err error) {
	var header http.Header
	header, err = cli.post("/subscriptions", sub, tenant)
	if err != nil {
		return
	}
	if val, ok := header["Location"]; ok {
		id = val[0]
	} else {
		err = errors.New("No location id")
	}
	return
}

// GetTypes requests all types
func (cli *Client) GetTypes(tenant string) (t []Type, err error) {
	var body []byte
	body, err = cli.get("/types", tenant)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &t)
	return
}

// post sends one post request to the context broker
func (cli *Client) post(path string, res interface{}, tenant string) (header http.Header, err error) {
	var js []byte
	js, err = json.Marshal(res)
	if err != nil {
		return
	}
	var request *http.Request
	request, err = http.NewRequest("POST", cli.url+path, bytes.NewReader(js))
	request.Header.Set("Content-Type", "application/json")
	if tenant != "" {
		request.Header.Set("Fiware-Service", tenant)
	}
	var resp *http.Response
	resp, err = cli.httpCli.Do(request)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusCreated {
		err = errors.New("http error: " + strconv.Itoa(resp.StatusCode))
	}
	header = resp.Header
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	return
}

// patch sends one patch request to the context broker
func (cli *Client) patch(path string, res interface{}, tenant string) (err error) {
	var js []byte
	js, err = json.Marshal(res)
	if err != nil {
		return
	}
	var request *http.Request
	request, err = http.NewRequest("PATCH", cli.url+path, bytes.NewReader(js))
	request.Header.Set("Content-Type", "application/json")
	if tenant != "" {
		request.Header.Set("Fiware-Service", tenant)
	}
	var resp *http.Response
	resp, err = cli.httpCli.Do(request)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusNoContent {
		err = errors.New("http error: " + strconv.Itoa(resp.StatusCode))
	}
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	return
}

// put sends one put request to the context broker
func (cli *Client) put(path string, res interface{}, tenant string) (err error) {
	var js []byte
	js, err = json.Marshal(res)
	if err != nil {
		return
	}
	var request *http.Request
	request, err = http.NewRequest("PUT", cli.url+path, bytes.NewReader(js))
	request.Header.Set("Content-Type", "application/json")
	if tenant != "" {
		request.Header.Set("Fiware-Service", tenant)
	}
	var resp *http.Response
	resp, err = cli.httpCli.Do(request)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusNoContent {
		err = errors.New("http error: " + strconv.Itoa(resp.StatusCode))
	}
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	return
}

// get sends one get request to the context broker and returns the answer
func (cli *Client) get(path string, tenant string) (body []byte, err error) {
	var request *http.Request
	request, err = http.NewRequest("GET", cli.url+path, nil)
	if tenant != "" {
		request.Header.Set("Fiware-Service", tenant)
	}
	var resp *http.Response
	resp, err = cli.httpCli.Do(request)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = errors.New("http error: " + strconv.Itoa(resp.StatusCode))
		ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		return
	}
	body, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	return
}

// delete sends one delete request to the context broker
func (cli *Client) delete(path string, tenant string) (err error) {
	var request *http.Request
	request, err = http.NewRequest("DELETE", cli.url+path, nil)
	if tenant != "" {
		request.Header.Set("Fiware-Service", tenant)
	}
	var resp *http.Response
	resp, err = cli.httpCli.Do(request)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusNoContent {
		err = errors.New("http error: " + strconv.Itoa(resp.StatusCode))
	}
	ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	return
}
