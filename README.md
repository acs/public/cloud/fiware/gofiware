# FIWARE Go client

This project implements a Go client for different [FIWARE](https://www.fiware.org/) components. Currently supported components are:

* [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/)

Components that will be supported in the future:

* Timeseries Data ([Quantumleap](https://quantumleap.readthedocs.io/en/latest/))
* [JSON IoT Agent](https://fiware-tutorials.readthedocs.io/en/latest/iot-agent-json/index.html#connecting-iot-devices)

## Usage

Have a look at `cmd/main.go` for some examples on how to use the client.

## Copyright

2020, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```text
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

* [Stefan Dähling](mailto:sdaehling@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  
