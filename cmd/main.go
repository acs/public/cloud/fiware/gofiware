/*
Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"

	"git.rwth-aachen.de/acs/public/cloud/fiware/gofiware/pkg/orion"
)

// custom attribute data
type cust struct {
	Data1 int
	Data2 string
}

func main() {
	// create client
	cli, err := orion.NewClient("http://localhost:1026")
	if err != nil {
		fmt.Println(err)
		return
	}

	// create an entity
	ent := orion.Entity{
		ID:         "ent1",
		Type:       "testentity",
		Attributes: make(map[string]orion.Attribute),
	}
	ent.Attributes["attr1"] = orion.Attribute{Value: 3, Type: "Integer"}
	ent.Attributes["attr2"] = orion.Attribute{Value: "hello", Type: "String"}
	c := cust{Data1: 2, Data2: "test"}
	ent.Attributes["attr3"] = orion.Attribute{Value: c, Type: "custom"}
	err = cli.PostEntity(ent, "")
	if err != nil {
		fmt.Println(err)
		return
	}

	// get entity
	var entGet orion.Entity
	entGet, err = cli.GetEntity("ent1", "")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(entGet)

	// get entity key-value
	var entKV orion.EntityKV
	entKV, err = cli.GetEntityKV("ent1", "")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(entKV)

	// get values of entity attributes
	var val []interface{}
	val, err = cli.GetEntityAttributes("ent1", "", "attr3", "attr1")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(val)

	// get attribute
	var attr orion.Attribute
	attr, err = cli.GetAttribute("ent1", "attr3", "")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(attr)

	// create another entity
	ent2 := orion.Entity{
		ID:         "ent2",
		Type:       "test2entity",
		Attributes: make(map[string]orion.Attribute),
	}
	err = cli.PostEntity(ent2, "")
	if err != nil {
		fmt.Println(err)
		return
	}

	// get all entities
	var ents []orion.Entity
	ents, err = cli.GetAllEntities("")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(ents)

	// get all entities of a type
	ents, err = cli.GetAllEntitiesType("testentity", "")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(ents)

	// Update attributes
	attrTemp := ent.Attributes["attr1"]
	attrTemp.Value = 42
	attrList := orion.AttributeList{Attributes: make(map[string]orion.Attribute)}
	attrList.Attributes["attr1"] = attrTemp
	err = cli.UpdateAttributes(ent.ID, attrList, "")
	if err != nil {
		fmt.Println(err)
		return
	}
	entGet, err = cli.GetEntity("ent1", "")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(entGet)

	// update entity
	err = cli.UpdateEntity(ent.ID, attrList, "")
	if err != nil {
		fmt.Println(err)
		return
	}
	entGet, err = cli.GetEntity("ent1", "")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(entGet)

	// get types
	var t []orion.Type
	t, err = cli.GetTypes("")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(t)

	// delete entities
	err = cli.DeleteEntity(ent.ID, "")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = cli.DeleteEntity(ent2.ID, "")
	if err != nil {
		fmt.Println(err)
		return
	}

	return
}
